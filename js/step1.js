
$('.steps__select').click (function() {
	const display_act = $('.steps__one_activated').css('display');
	const display_deact = $('.steps__one_deactivated').css('display');
	$('.steps__one_activated').css('display', display_act === 'block' ? 'none' : 'block');
	$('.steps__one_deactivated').css('display', display_deact === 'block' ? 'none' : 'block');
});

function Popup(options){
    this.modal = document.querySelector(options.modal);
    this.overlay = document.querySelector(options.overlay);
    
    var popup = this;
    
    this.open2 = function(content){
        popup.modal.innerHTML = content;
        popup.overlay.classList.add('open2');
        popup.modal.classList.add('open2');
    }
    
    this.close = function(){
        popup.overlay.classList.remove('open2');
        popup.modal.classList.remove('open2');
    }
    
    this.overlay.onclick = popup.close;
}

window.onload = function(){
    var p = new Popup({
        modal: '.modal',
        overlay: '.overlay'
    });
    
    

    document.querySelector('.callme').onclick = function(){
        var form = document.querySelector('.for-callme-popup');
        p.open2(form.innerHTML);
    };
}