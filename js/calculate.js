$( document ).ready(function() {

    const clichePrice = 500;
    let termPrice = 0;
    let deliveryPrice = 0;

    const selectTerm = document.getElementById("term-option");
    const selectDelivery = document.getElementById("delivery-option");
    const termCost = document.getElementById("term-total");
    const deliveryCost = document.getElementById("delivery-total");
    const furniturePrice = Number(document.getElementById("furniture-total").innerText);
    const totalSum = document.getElementById("sum-total");

    const setTotalSum = () => {
        totalSum.innerText = clichePrice + deliveryPrice + termPrice + furniturePrice;
    };

    const setFormValues = (valueName) => {
        const valueTotal= document.getElementById(valueName + "-total");
        $("#" + valueName + "-value").val(valueTotal.textContent);
    };

    const setAllFormValues = () => {
        $("#cliche-value").val(clichePrice);
        setFormValues("furniture");
        setFormValues("term");
        setFormValues("delivery");
        setFormValues("sum");
        //
        // console.log("del=", $("#delivery-value").val());
        // console.log("term=", $("#term-value").val());
        // console.log("furn=", $("#furniture-value").val());
        // console.log("sum=", $("#sum-value").val());
        // console.log("cliche=", $("#cliche-value").val());
    };

    setTotalSum();
    setAllFormValues();

    selectTerm.addEventListener("change", function() {
        const selectedValue = selectTerm.options[selectTerm.selectedIndex].value;
        if (selectedValue === 'urgent'){
            termPrice = clichePrice / 2;
        } else if (selectedValue === "" || selectedValue === "standard"){
            termPrice = 0;
        }
        termCost.innerText = termPrice;
        setTotalSum();
        setAllFormValues();
    });

    selectDelivery.addEventListener("change", function() {
        const selectedValue = selectDelivery.options[selectDelivery.selectedIndex].value;
        if (selectedValue === "courier"){
            deliveryPrice = 300;
        } else if (selectedValue === "urgent"){
            deliveryPrice = 700;
        } else if (selectedValue === "" || selectedValue === "pickup"){
            deliveryPrice = 0;
        }
        deliveryCost.innerText = deliveryPrice;
        setTotalSum();
        setAllFormValues();
    });


});