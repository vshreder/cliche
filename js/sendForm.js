$(document).ready(function ($) {

    $("#order-form").submit(function (e) {
        e.stopImmediatePropagation();
        console.log("press button");
        const str = $(this).serialize();
        let result = "";

        $.ajax({
            type: "POST",
            url: "order.php",
            data: str,
            success: function (msg) {
                if (msg === "OK") {
                    result = '<div class="notification_ok">Спасибо, ваше сообщение было отправлено. Ожидайте звонка</div>';
                    $("#order-form").hide();
                } else {
                    result = msg;
                }
                $('#notification').html(result);
            }
        });
        return false;
    });

});