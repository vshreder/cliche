<?php

$post = (!empty($_POST)) ? true : false;

if($post)
{
    $recipient = "admin.service@libertate.ru";  // здесь адрес заменить на почту, на которую должны приходить сообщения
    $email = "no-reply@gmail.com";
    $name = "Default User";
    $phone = "800800800800";
    $headers = "Content-type: text/plain; charset=utf-8 \r\n";
    $delivery = array(
        $deliveryType = htmlspecialchars($_POST['delivery-option']),
        $deliveryPrice = htmlspecialchars($_POST['delivery-value'])
    );

    $term = array(
        $termType = htmlspecialchars($_POST['term-option']),
        $termPrice = htmlspecialchars($_POST['term-value'])
    );

    $furniture = htmlspecialchars($_POST['furniture-value']);
    $cliche = htmlspecialchars($_POST['cliche-value']);
    $sum = htmlspecialchars($_POST['sum-value']);

        $subject ="Заказ печати";
        $message ="Произошел заказ печати со следующими параметрами: \n
        Стоимость клише: ${cliche}\n
        Стоимость оснастки: ${furniture}\n
        Тип доставки: ${delivery[0]}\n
        Стоимость достаки: ${delivery[1]}\n
        Тип срочности: ${term[0]}\n
        Стоимость срочности: ${term[1]}\n
        Общая стоимость: ${sum}\n
        ";
        $mail = mail($recipient, $subject, $message,

        $headers."From: ".$name." <".$phone."> ");

    if($mail)
    {
        echo 'OK';
    } else {
        echo '<div class="notification_error">Произошла ошибка</div>';
    }
}
?>